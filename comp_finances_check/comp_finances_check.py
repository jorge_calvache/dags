"""This file contains the Compensation-Finances-alignment DAG."""

import airflow
import os
from datetime import datetime, timedelta
#from lib import slack_lib
from airflow.models import DAG
from airflow.operators.dummy_operator import DummyOperator
from airflow.contrib.operators.snowflake_operator import SnowflakeOperator
#from rappiflow.utils.k8s_settings import get_k8s_executor_resource_config


daily_args = {
    'owner': 'jorge.calvache',
    'depends_on_past': False,
    'start_date': airflow.utils.dates.days_ago(1),
    'email_on_failure': False,
    'email_on_retry': False,
    #'on_failure_callback': slack_lib.alert_failure_slack_channel,
    'retries': 1,
    'retry_delay': timedelta(minutes=1),
    #'executor_config': get_k8s_executor_resource_config(resource_request='S', resource_limits='M')
}

dir_path = os.path.join(os.path.dirname(os.path.realpath(__file__)),"queries")

# =========================================================
# alineacion_finanzas_compensaciones
# =========================================================


finance_compensation_alignment = DAG(
    dag_id='finance_compensation_alignment',
    default_args=daily_args,
    schedule_interval='0 7 * * *',
    catchup=False,
    max_active_runs=1,
    template_searchpath=dir_path
)

dummy_start = DummyOperator(
    task_id="dummy_start",
    dag=finance_compensation_alignment
)


finance_compensation_alignment_events = [
    SnowflakeOperator(
        task_id='finance_compensation_alignment_events_' + country,
        dag=finance_compensation_alignment,
        snowflake_conn_id='snowflake_conn_string',
        sql="Finanzas_Ops_Reactivas.sql",
        params={
            'country': country,
            'start_date': (datetime.now() - timedelta(days=15)).strftime('%Y-%m-%d'),
            'end_date': datetime.now().strftime('%Y-%m-%d'),
        },
        warehouse='OPERATIONS',
        database='FIVETRAN',
        role='OPS_GLOBAL_WRITE_ROLE',
        trigger_rule='all_done',
    ) for country in ['AR', 'BR', 'CL', 'CO', 'CR', 'EC', 'MX', 'PE', 'UY']
]


dummy_start >> \
    finance_compensation_alignment_events 
