/*variables fecha */
--set rangos = dateadd('month', -13, date_trunc('month', current_date));
/*Inicio de query*/
//Valores compensado por order

merge into ops_global.user_segmentation_events_{{params.country}} as target
using
--create or replace table ops_global.user_segmentation_events as
(
with compensations as 
     (
    select 
           '{{params.country}}'                                                                                            as country,
           order_id,
           sum(amount)                                                                                              as compensation_value_local,
           sum(amount/trm_fixed.trm)                                                                                as compensation_value_usd,
           sum(case when coalesce(source, 'null') in ('lupe_app') then amount else 0 end / trm_fixed.trm)           as agent_compensation_usd,
           sum(case when coalesce(source, 'null') in ('lupe_app') then amount else 0 end)                           as agent_compensation_local,
           sum(case when coalesce(source, 'null') in ('ticket-automation') then amount else 0 end/ trm_fixed.trm)   as automatic_compensation_usd,
           sum(case when coalesce(source, 'null') in ('ticket-automation') then amount else 0 end)                  as automatic_compensation_local

    from {{params.country}}_pg_ms_compensations_public.compensations_log
        left join global_finances.trm_fixed on trm_fixed.country_code = '{{params.country}}'
    where 1=1
      and is_valid_satus 
    group by 1,2

    ), 

  operacion as 
  (
      select 
              '{{params.country}}'                                                              as country,
              order_id,
              count(distinct source)                                            as source_diferentes,
              max(source) as max_s,
              case when source_diferentes > 1 then 'Automation + Manual' 
                   when max_s = 'ticket-automation' then 'Automation'
                   when max_s = 'lupe_app' then 'Manual' end                    as operation_type

      from {{params.country}}_pg_ms_compensations_public.compensations_log
      where source in ('ticket-automation','lupe_app')
        and is_valid_satus 
      group by 1,2
  ),

  exit_tipification as (

select 
        compensations_log.order_id,
        compensations_log.additional_data_input,
        compensations_log.created_at,
        case when compensations_type.key ilike '%manual%' then coalesce(replace(additional_data_input['reasonId'],'"','')::text , replace (additional_data_input['reason'],'"','')::text, replace (additional_data_input['state'],'"','')::text)
                 when compensations_type.key ilike '%automatic%' then coalesce(SUBSTRING(replace (additional_data_input['reason'],'"',''),
                                                                  charindex('type:',replace (additional_data_input['reason'],'"','')) + LEN('type:'), 
                                                                  LEN(replace (additional_data_input['reason'],'"',''))) , RESPONSE_REPORT_COMPENSATION::text)
            end as exit_tipification_,
            
        case
                when exit_tipification_ ilike '%sn_org_mp.sn_org_pa.sn_org_pme%' then 'PRODUCT_POOR_CONDITION' -- producto mal estado
                when exit_tipification_ ilike '%sn_org_mp.sn_org_pa.sn_org_tpd%' then 'DIFFERENT_PRODUCT' --trajeron producto diferente
                when exit_tipification_ ilike '%sn_org_mp.sn_org_pa.sn_org_pi%' then 'INCOMPLETE_ORDER' --PEDIDO INCOMPLETO
                when exit_tipification_ ilike '%sn_org_mp.sn_org_pa.sn_org_fg%' then 'FRESCURA_GARANTIZADA'
                when exit_tipification_ ilike '%sn_org_mp.sn_org_pa.sn_org_mpnfe%' then 'NOT_DELIVERED_ORDER' --mi pedido no fue entregado
                else exit_tipification_
          end exit_tipification,
        count(distinct source) as source_diferentes,
        max(source) as max_s,
        case when source_diferentes > 1 then 'Automation + Manual' 
             when max_s = 'ticket-automation' then 'Automation'
             when max_s = 'lupe_app' then 'Manual' end as tipo_operacion
from {{params.country}}_PG_MS_COMPENSATIONS_PUBLIC.compensations_log
  left join {{params.country}}_PG_MS_COMPENSATIONS_PUBLIC.compensations_type ON compensations_log.compensations_type_id = compensations_type.id
where source in ('ticket-automation','lupe_app')
  and is_valid_satus 
        
group by 1,2,3,4,5
qualify row_number() over (partition by compensations_log.order_id order by compensations_log.created_at) = 1


  ),
  
  recovery as 
  (  
 select country, 
                 to_timestamp(coalesce (case when class in (10,19,32,11) then date_trunc('day',TRANSACTION_CREATED_AT::date) end,   date_trunc('day',order_created_at::date)))   created_at,
                 null city_name,
                 null order_id,
                 null vertical_group,
                 null vertical_sub_group,
                 null brand_id,
                 null store_id,
                 null gmv,
                 null gmv_usd,
                 null application_user_id,
                 null kustomer_created_at,
                 null kustomer_conversation_id,
                 null level_1,
                 null level_2,
                 null exit_tipification,
                 null nss_x_ratings,
                 null no_of_ratings,
                 null is_automated,
                 null reopen,
                 null ticketAutomationExitRule,
                 null iteration, 
                 null ticket_type_nodes,
                 null key_,
                 null automatic_solution_type,
                 null ticket_type,
                 null count_to_gmv,
                 null loyalty,
                 null rfm_segment,
                 null finance_user_type, 
                 null operation_type,
                 null tipologia,
                 null compensation_value_local,
                 null compensation_value_usd,
                 null agent_compensation_usd,
                 null agent_compensation_local,
                 null automatic_compensation_usd,
                 null automatic_compensation_local,
                 null Rappi_credits_debited,
                 null Ally_Compensations,

                        sum(case when coalesce(class, null) in (32) then NET_SPEND_USD else 0 end)  as Ally_Compensations_Adjustment,
    
           null Order_modification_discounts,
           null Rappi_Credits_not_Debited,
           null max_transaction_created_at_refunds,
                        sum(case when coalesce(class, null) in (19) and order_id = -9999 then NET_SPEND_USD else 0 end)  as Refunds,
           null team,
           null gmv_after_7_days,
           null orders_after_7_days,
           null churn_7,
           null gmv_after_14_days,
           null orders_after_14_days,
           null churn_14,
           null gmv_after_21_days,
           null orders_after_21_days,
           null churn_21,
           null gmv_after_28_days,
           null orders_after_28_days,
           null churn_28,
           null gmv_after_7_days_organic,
           null orders_after_7_days_organic,
           null churn_7_organic,
           null gmv_after_14_days_organic,
           null orders_after_14_days_organic,
           null churn_14_organic,
           null gmv_after_21_days_organic,
           null orders_after_21_days_organic,
           null churn_21_organic,
           null gmv_after_28_days_organic,
           null orders_after_28_days_organic,
           null churn_28_organic,
           null orders,
           null users,
           null tickets

          from fivetran.global_finances.global_real_spend
          where (order_created_at::date >= '2021-01-01'  or TRANSACTION_CREATED_AT::date>= '2021-01-01')
          and assumed_by not in ('Rappi')
          and FINANCIALS_SUB_CATEGORY = 'COMPENSATIONS' 
and FINANCIALS_CATEGORY = 'OPS'
and country = '{{params.country}}'
and created_at between '{{params.start_date}}'::date and '{{params.end_date}}'::date
group by 1,2

  ),    
recovery_v2 as
  (
          select country,
                 order_id,
                 --team,
                 --coalesce (case when class in (10,19,32,11) then date_trunc('day',TRANSACTION_CREATED_AT::date) end,   date_trunc('day',order_created_at::date))   day,
                        sum(case when coalesce(class, null) in (2)  and team not ilike '%late_%' then NET_SPEND_USD else 0 end)  as Rappi_credits_debited,
                        sum(case when coalesce(class, null) in (11) then NET_SPEND_USD else 0 end)  as Ally_Compensations,
                        sum(case when coalesce(class, null) in (32) then NET_SPEND_USD else 0 end)  as Ally_Compensations_Adjustment,
                        sum(case when coalesce(class, null) in (26) then NET_SPEND_USD else 0 end)  as Order_modification_discounts,
                        sum(case when coalesce(class, null) in (24) then NET_SPEND_USD else 0 end)  as Rappi_Credits_not_Debited,
                        sum(case when coalesce(class, null) in (19) then NET_SPEND_USD else 0 end)  as Refunds,
                        sum(case when coalesce(class, null) in (2)  and team ilike '%late_%' then NET_SPEND_USD else 0 end)  as Rappi_credits_debited_proactives
                 --sum(net_spend_usd) as net_spend_usd
          from fivetran.global_finances.global_real_spend
          where (order_created_at::date >= '2021-01-01'  or TRANSACTION_CREATED_AT::date>= '2021-01-01')
          and assumed_by not in ('Rappi')
          and FINANCIALS_SUB_CATEGORY = 'COMPENSATIONS'
and FINANCIALS_CATEGORY = 'OPS'
and country = '{{params.country}}'
--and day between '{{params.start_date}}'::date and '{{params.end_date}}'::date
group by 1,2--,3

),

recovery_v3 as (

select  rs.country,
        rs.order_id,
        max(TRANSACTION_CREATED_AT) as max_transaction_created_at_refunds,
                        sum(case when coalesce(class, null) in (19) then NET_SPEND_USD else 0 end)  as Refunds
          from fivetran.global_finances.global_real_spend rs
          where (order_created_at::date >= '2021-01-01'  or TRANSACTION_CREATED_AT::date>= '2021-01-01')
          and assumed_by not in ('Rappi')
          and FINANCIALS_SUB_CATEGORY = 'COMPENSATIONS'
and FINANCIALS_CATEGORY = 'OPS'
and country = '{{params.country}}'
and rs.class  = 19
group by 1,2

),


node_info as(
  select  '{{params.country}}' COUNTRY,
          LOGS_COMPENSATED_DETAIL.log_compensated_data_id,
          coalesce(nodes.key,get(processed_toppings,0):output_node:key) key_,
          iff(split_part(key_,'_',-1) = '06', split_part(key_,'_',-4), split_part(key_,'_',-1)) as iteration,
          split_part(key_,'_',1) as vertical,
          split_part(key_,'_',2) as ticket_type,
          MAX(try_to_numeric(coalesce(nodes.rappi_comp,get(processed_toppings,0):output_node:rappi_comp),2,1)) rappi_comp_,
          MAX(try_to_numeric(coalesce(nodes.partner_comp,get(processed_toppings,0):output_node:partner_comp),2,1)) partner_comp_,
          count(1)
  from {{params.country}}_PG_MS_COMPENSATION_CALCULATOR_PUBLIC.LOGS_COMPENSATED_DETAIL
    left join {{params.country}}_PG_MS_COMPENSATION_CALCULATOR_PUBLIC.nodes on LOGS_COMPENSATED_DETAIL.output_node_id = nodes.id
                                                              and nodes.type = 'OUTPUT' /*and nodes.deleted_at is null*/
  group by 1,2,3,4,5,6
),
info as (
select                      logs_compensated_data.*,
                             node_info.*,
                             logs.ticket_id,
                             logs.order_id,
                             ROW_NUMBER()
                                     OVER (PARTITION BY node_info.COUNTRY, logs.ticket_id ORDER BY logs_compensated_data.CREATED_AT) AS RW,
          (CASE WHEN KEY_::TEXT LIKE '%it1_d_04_06%' then replace(KEY_, 'it1_d_04_06', '')   else
            (CASE WHEN KEY_::TEXT LIKE '%it2_d_04_06%' then replace(KEY_, 'it2_d_04_06', '')  else
                (CASE WHEN KEY_::TEXT LIKE '%it3_d_04_06%' then replace(KEY_, 'it3_d_04_06', '')  else replace(KEY_, ITERATION, '')
             end)end)end) as new_key
                      from {{params.country}}_PG_MS_COMPENSATION_CALCULATOR_PUBLIC.logs_compensated_data
                               left join node_info on logs_compensated_data.id = node_info.log_compensated_data_id and
                                                      node_info.country = '{{params.country}}'
                               left join {{params.country}}_PG_MS_COMPENSATION_CALCULATOR_PUBLIC.logs
                                         on logs_compensated_data.log_id = logs.id
          where log_id is not null
          and logs.ticket_id is not null
          qualify rw = 1
          ),

inflow_count as
              (select country,
                      ticket_id,
                      sum(case when inflow_event != 'agent_reassingment' then 1 else 0 end)       as total_inflow,
                      sum(case when inflow_event like '%reopen%' then 1 else 0 end)               as reopen_count,
                      sum(case when inflow_event = 'non_live_queue_escalation' then 1 else 0 end) as queue_escalations,
                      sum(case
                              when inflow_event in ('non_live_queue_escalation', 'agent_reassingment') then 1
                              else 0 end)                                                         as quantity_transf,
                      sum(case when automation then 1 else 0 end)                                 as automated_resolutions,
                      sum(case when automation_type = 'decision manager' then 1 else 0 end)       as aut_res_type_decision_manager,
                      sum(case when automation_type = 'live automation' then 1 else 0 end)        as aut_res_type_live_automation,
                      sum(case when automation_type = 'order canceled' then 1 else 0 end)         as aut_res_type_order_canceled,
                      sum(case when automation_type = 'order closed' then 1 else 0 end)           as aut_res_type_order_closed,
                      sum(case when automation_type = 'whatsapp automation' then 1 else 0 end)    as aut_res_type_whatsapp_automation,
                      sum(case when automation_type = 'workflow automation' then 1 else 0 end)    as aut_res_type_workflow_automation

               from ops_global.cs_inflow
               where country = '{{params.country}}'
               group by 1, 2
               
              ),

tickets as (
    select  nltdv2.country,
            nltdv2.order_id,
            nltdv2.kustomer_created_at,
            nltdv2.kustomer_conversation_id,
            nltdv2.level_1,
            nltdv2.level_2,
            nltdv2.application_user_id,
            case when survey_rating = 5 then 1
                 when survey_rating = 4 then 0
                 when survey_rating < 4 then -1
                 else null end                                                  as nss_x_ratings,
            case when survey_rating > 0 then 1
                 else null end                                                  as no_of_ratings,
            ci.automated_resolutions > 0                                        as is_automated,
            ci.reopen_count                                                     as reopen,
            coalesce(c.custom:ticketAutomationExitRuleStr::text,
                c.custom:dMexitruleTree::text)                                  as ticketAutomationExitRule,
            --upper(replace(c.custom['loyaltyStr'], '"', ''))                     as loyalty,
            coalesce(replace(c.custom['rfmSegmentStr'], '""', ''), segment_rfm) as rfm_segment,
            info.iteration, 
            info.ticket_type                                                    as ticket_type_nodes,
            info.new_key,
            c.automatic_solution_type,
                                    case
                                    when LEVEL_2  = 'Request not delivered' or LEVEL_2  ILIKE '%Order didn'  then '1) Pedido no llegó'
                                    when LEVEL_2  = 'Missing product' then '2) Falta un item'
                                    when LEVEL_2  = 'different product' then '4) Item equivocado'
                                    when LEVEL_2  in ('Product in poor condition','Return of products','Non compesated') then '3) Producto en mal estado'
                                    when LEVEL_1 in ('Disagree with Charge',
                                                                'Order status / delay',
                                                                'Ecommerce',
                                                                'Emergency',
                                                                'New Bets',
                                                                'New Rts',
                                                                'Doubts with the Live payment',
                                                                'Another ally communicates with PK live',
                                                                'Problems with RT',
                                                                'CPGs',
                                                                ' Returns',
                                                                'Transactional application',
                                                                'Debt',
                                                                'Problems with app',
                                                                'Calendar, menu and price update',
                                                                'COVID 19',
                                                                'Information request',
                                                                'Account User',
                                                                'Congratulation',
                                                                'Partner downtime',
                                                                'Problems with Promotions',
                                                                'Regulatory entities',
                                                                'Courier Blocked',
                                                                'Adjustments during order',
                                                                'Issues with app /tech',
                                                                'Problems with order',
                                                                'Restaurants',
                                                                'Soporte Mi Tienda',
                                                                'Rappipay',
                                                                'Complain',
                                                                'Price Diff / Dispersions',
                                                                'Adjustments During Order',
                                                                'Problem with RT/Shpper/Picker',
                                                                'RT Payment',
                                                                'Store Close',
                                                                'RT assignment',
                                                                'Suggestion or opinion',
                                                                'Delivery schedule settings',
                                                                'Partner inactivity',
                                                                'Cancellations NL',
                                                                'RT inactivity',
                                                                'Problem Store',
                                                                'Account RT',
                                                                'Travel',
                                                                'Automatizaciones',
                                                                'Out of hour business',
                                                                'Support My Store',
                                                                'unidentified',
                                                                'Account User ',
                                                                'Adjusting during order',
                                                                'Assign or release RT',
                                                                'Store Close ') then '6) Compensaciones Fuera'
                                    else '5) Other' end                                    as ticket_type

            


    
    from ops_global.non_live_tickets_details_v2 nltdv2
        left join inflow_count ci
            on (nltdv2.kustomer_conversation_id = ci.ticket_id and nltdv2.country = '{{params.country}}')
            
        left join fivetran.co_pg_ms_kustomer_etl_public.conversations c 
            on upper(case when substr(lower(trim(c.country)), 1, 2) not in
                ('ar', 'br', 'cl', 'co', 'cr', 'ec', 'mx', 'pe', 'uy') then null
                    else substr(lower(trim(c.country)), 1, 2) end) = '{{params.country}}'
                        and c.kustomer_conversation_id = nltdv2.kustomer_conversation_id

        left join info on info.ticket_id = nltdv2.kustomer_conversation_id and info.country = '{{params.country}}'


           where nltdv2.country = '{{params.country}}' 
                and nltdv2.is_defect 
                and nltdv2.user_type = 'Customer' 
                and nltdv2.KUSTOMER_CREATED_AT::DATE between '{{params.start_date}}'::date and '{{params.end_date}}'::date
        qualify row_number() over (partition by nltdv2.country, nltdv2.order_id  order by nltdv2.KUSTOMER_CREATED_AT) = 1

),

  user_segmentation_adjustment as (
    select
        id,
        country,
        case
            when category in ('DIAMANTE') then 'DIAMOND'
            when category in ('OURO', 'ORO') then 'GOLD'
            when category in ('PRATA', 'PLATA')  then 'SILVER'
            when category in ('BRONZE', 'BRONCE')  then 'BRONZE'
        end as loyalty,
        start_date,
        end_date
    from GLOBAL_FINANCES.GLOBAL_APPLICATION_USERS_HIST 
  where country = '{{params.country}}' 
    ),

//todas las ordenes

pre_previa as (
    select gr_orders.created_at,
           city.city_name,
           gr_orders.order_id,
           gr_orders.vertical_group,
           gr_orders.vertical_sub_group,
           gr_orders.brand_id,
           gr_orders.store_id,
           gr_orders.gmv,
           gr_orders.gmv / trm_fixed.trm                                                    as gmv_usd,
           coalesce(gr_orders.application_user_id, tickets.application_user_id)             as application_user_id,
           tickets.kustomer_created_at,
           tickets.kustomer_conversation_id,
           tickets.level_1,
           tickets.level_2,
           exit_tipification.exit_tipification,
           tickets.nss_x_ratings,
           tickets.no_of_ratings,
           tickets.is_automated,
           tickets.reopen,
           tickets.ticketAutomationExitRule,
           tickets.iteration, 
           tickets.ticket_type_nodes,
           tickets.new_key,
           tickets.automatic_solution_type,
           tickets.ticket_type,
           gr_orders.count_to_gmv,
           hist.category                                                       as loyalty,
           coalesce(hist.segment_rfm, tickets.rfm_segment)                     as rfm_segment,
           hist.type_user                                                      as finance_user_type, 
           operacion.operation_type,

           compensations.compensation_value_local,
           compensations.compensation_value_usd,
           compensations.agent_compensation_usd,
           compensations.agent_compensation_local,
           compensations.automatic_compensation_usd,
           compensations.automatic_compensation_local,

           recovery_v2.Rappi_credits_debited,
           recovery_v2.Ally_Compensations,
           0 Ally_Compensations_Adjustment,
           recovery_v2.Order_modification_discounts,
           recovery_v2.Rappi_Credits_not_Debited,
           recovery_v3.max_transaction_created_at_refunds,
           coalesce(recovery_v2.Refunds, /*tbl_ue_refunds.refund_compensaciones,*/ recovery_v3.Refunds) as refunds,
           case when upper(exit_tipification) ilike 'FREE_CANCELLATION' then 'Refunds'
                when upper(exit_tipification) ilike 'CANCELLATION_AUTOMATION' then 'Compensaciones'
                when upper(exit_tipification) ilike 'CANCELLATION_DELAY_ETA' then 'Compensaciones'
                when upper(exit_tipification) ilike 'CANCELLATION_HIGH_DEMAND_RESTAURANT' then 'Compensaciones'
                when upper(exit_tipification) ilike 'CS_CANCELLATION_LOST_PROMOTION' then 'Growth'
                when upper(exit_tipification) ilike 'CS_LIVE_USER_RETENTION' then 'Compensaciones'
                when upper(exit_tipification) ilike 'RE_ORDER_INCOMPLETE' then 'Compensaciones'
                when upper(exit_tipification) ilike 'RE_ORDER_POOR_CONDITION' then 'Compensaciones'
                when upper(exit_tipification) ilike 'RE_ORDER_DIFFERENT' then 'Compensaciones'
                when upper(exit_tipification) ilike 'RE_ORDER_DID_NOT_ARRIVE' then 'Compensaciones'
                when upper(exit_tipification) ilike 'CREDITS_DID_NOT_APPLIED' then 'Growth'
                when upper(exit_tipification) ilike 'PRIME_DID_NOT_APPLIED' then 'Growth'
                when upper(exit_tipification) ilike 'DELAY_IN_DELIVERY' then 'Compensaciones'
                when upper(exit_tipification) ilike 'PROMO_DID_NOT_APPLY' or exit_tipification ilike 'Promotion did not apply' then 'Growth'
                when upper(exit_tipification) ilike 'PRICE_DIFFERENCE' then 'Diferencia de precios'
                when upper(exit_tipification) ilike 'OVER_CHARGED' then 'Refunds'
                when upper(exit_tipification) ilike 'RAPPI_PAY_PROMOTION_NOT_APPLIED' then 'Pay'
                when upper(exit_tipification) ilike 'CALL_CENTER_REACTIVATION' then 'Growth'
                when upper(exit_tipification) ilike 'DIFFERENT_PRODUCT' then 'Compensaciones'
                when upper(exit_tipification) ilike 'PRODUCT_POOR_CONDITION' or exit_tipification ilike 'Product in poor condition' or exit_tipification ilike 'Producto en mal estado' then 'Compensaciones'
                when upper(exit_tipification) ilike 'INCOMPLETE_ORDER' or exit_tipification ilike 'Pedido incompleto' then 'Compensaciones'
                when upper(exit_tipification) ilike 'NOT_DELIVERED_ORDER'  or exit_tipification ilike 'Order not delivered' then 'Compensaciones'
                when upper(exit_tipification) ilike 'NOT_APPLIED_REFUND' then 'Refunds'
                when upper(exit_tipification) ilike 'RT_KEPT_THE_CHANGE' then 'Compensaciones'
                when upper(exit_tipification) ilike 'RT_CHARGED_IN_CASH_IT_WAS_CC' then 'Compensaciones'
                when upper(exit_tipification) ilike 'TRAVEL_GLOBAL_OFFER_NOT_APPLIED' then 'Growth'
                when upper(exit_tipification) ilike 'TRAVEL_RAPPICREDIT_REACTIVATION_SUCCESS' then 'Growth'
                when upper(exit_tipification) ilike 'TRAVEL_CHANGE_IN_PROVIDER_RESERVATION' then 'Travel'
                when upper(exit_tipification) ilike 'TRAVEL_REVERSION_AND_RETRACT' then 'Travel'
                when upper(exit_tipification) ilike 'TRAVEL_NO_EMISSION_RESERVATION' then 'Travel'
                when upper(exit_tipification) ilike 'TRAVEL_INCOMPLETE_RESERVATION' then 'Travel'
                when upper(exit_tipification) ilike 'TRAVEL_REFUND' then 'Travel'
                when upper(exit_tipification) ilike 'DONATION_REFUND' then 'Donaciones'
                when exit_tipification ilike 'Reactivation' then 'Growth'
                when exit_tipification ilike 'User retention' then 'Growth'
                when exit_tipification ilike 'CS_CANCELLATION_USER_RETENTION' then 'Growth'
                when exit_tipification ilike 'Fraud - Refund for user fraud' then 'Refunds'
                when kustomer_conversation_id is null then 'Sin Ticket' end
                                                                                                            as team

           /*case when datediff(day,gr_orders.created_at, lead(gr_orders.created_at,2) over (partition by gr_orders.APPLICATION_USER_ID order by gr_orders.created_at)) <= 28
                            then 1 else 0 end                                               as ORDERS_AFTER_28_DAYS_2_ORDER,
           case when coalesce(ORDERS_AFTER_28_DAYS_2_ORDER,0) = 0  then 1 else 0 end        as churn_28_2_ORDER*/

           
        from global_finances.{{params.country}}_orders gr_orders

            /*left join global_finances.global_application_users_hist  hist
                                on hist.country = '{{params.country}}'
                                       and hist.id = gr_orders.application_user_id
                                       and gr_orders.created_at::timestamp between hist.start_date::timestamp and coalesce(hist.end_date::timestamp, current_timestamp)*/

            left join (select *,
                              timeadd(second, -1, case when mc_actual = 1 then current_date + 1 else end_date end ) end_date_
                                      from global_finances.global_application_users_hist 
                                      /*qualify  rank() over (partition by ID, segment_rfm, category, type_user order by START_DATE desc) = 1*/) hist
                              on hist.country = '{{params.country}}'
                                       and hist.id = gr_orders.application_user_id
                                       and gr_orders.created_at::timestamp between hist.start_date::timestamp and end_date_::timestamp

            left join  fivetran.global_finances.tbl_dim_geography_t1 city
                on gr_orders.microzone_id = city.microzone_code
                    and city.country_index = '{{params.country}}'

            left join compensations on compensations.order_id = gr_orders.order_id

            left join tickets on tickets.order_id = gr_orders.order_id

            /*left join user_segmentation_adjustment usa on usa.id = gr_orders.application_user_id
                         and usa.country = '{{params.country}}'
                         and gr_orders.created_at::date between usa.start_date::date and usa.end_date::date*/
                         
            --left join recovery on recovery.country = '{{params.country}}' and gr_orders.created_at::date between recovery.order_created_at::date and recovery.day::date and gr_orders.order_id = recovery.order_id

            left join recovery_v2 on recovery_v2.order_id = gr_orders.order_id and recovery_v2.country = '{{params.country}}' 

            left join exit_tipification on exit_tipification.order_id = gr_orders.order_id 

            left join operacion on operacion.order_id = gr_orders.order_id

            left join global_finances.trm_fixed on trm_fixed.country_code = '{{params.country}}'

            --left join global_finances.tbl_ue_refunds on tbl_ue_refunds.country_index = '{{params.country}}' and tbl_ue_refunds.order_id = gr_orders.order_id

            left join recovery_v3 on recovery_v3.order_id = gr_orders.order_id and recovery_v3.country = '{{params.country}}' 


            
    where 1=1
    and gr_orders.created_at between '{{params.start_date}}'::date and '{{params.end_date}}'::date
    and gr_orders.count_to_gmv
        --qualify row_number() over (partition by gr_orders.order_id order by gr_orders.created_at) = 1

),

previa as
    (
      select pre_previa.*,
              coalesce(sum(case when datediff(day,pre_previa.created_at, global_orders.created_at) <= 7 then global_orders.gmv_usd else 0 end),0) as gmv_after_7_days,
              count(case when datediff(day,pre_previa.created_at, global_orders.created_at) <= 7 then global_orders.gmv_usd end) as orders_after_7_days,
              case when orders_after_7_days  = 0 then 1 else 0 end as churn_7,
              coalesce(sum(case when datediff(day,pre_previa.created_at, global_orders.created_at) <= 14 then global_orders.gmv_usd else 0 end),0) as gmv_after_14_days,
              count(case when datediff(day,pre_previa.created_at, global_orders.created_at) <= 14 then global_orders.gmv_usd end) as orders_after_14_days,
              case when orders_after_14_days  = 0 then 1 else 0 end as churn_14,
              coalesce(sum(case when datediff(day,pre_previa.created_at, global_orders.created_at) <= 21 then global_orders.gmv_usd else 0 end),0) as gmv_after_21_days,
              count(case when datediff(day,pre_previa.created_at, global_orders.created_at) <= 21 then global_orders.gmv_usd end) as orders_after_21_days,
              case when orders_after_21_days = 0 then 1 else 0 end as churn_21,
              coalesce(sum(case when datediff(day,pre_previa.created_at, global_orders.created_at) <= 28 then global_orders.gmv_usd else 0 end),0) as gmv_after_28_days,
              count(case when datediff(day,pre_previa.created_at, global_orders.created_at) <= 28 then global_orders.gmv_usd end) as orders_after_28_days,
              case when orders_after_28_days = 0 then 1 else 0 end as churn_28

      from pre_previa
      left join global_finances.global_orders on  global_orders.country = '{{params.country}}'
                      and global_orders.application_user_id = pre_previa.application_user_id
                      and global_orders.order_state not ilike '%cance%'
                      and global_orders.created_at > pre_previa.created_at
                      and global_orders.created_at <= pre_previa.created_at + interval '28 days'
      group by 1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31,32,33,34,35,36,37,38,39,40,41,42,43,44

),

previa_final as
    (
      select previa.*,
              coalesce(sum(case when datediff(day,previa.created_at, global_orders.created_at) <= 7 then global_orders.gmv_usd else 0 end),0) as gmv_after_7_days_organic,
              count(case when datediff(day,previa.created_at, global_orders.created_at) <= 7 then global_orders.gmv_usd end) as orders_after_7_days_organic,
              case when orders_after_7_days_organic  = 0 then 1 else 0 end as churn_7_organic,
              coalesce(sum(case when datediff(day,previa.created_at, global_orders.created_at) <= 14 then global_orders.gmv_usd else 0 end),0) as gmv_after_14_days_organic,
              count(case when datediff(day,previa.created_at, global_orders.created_at) <= 14 then global_orders.gmv_usd end) as orders_after_14_days_organic,
              case when orders_after_14_days_organic  = 0 then 1 else 0 end as churn_14_organic,
              coalesce(sum(case when datediff(day,previa.created_at, global_orders.created_at) <= 21 then global_orders.gmv_usd else 0 end),0) as gmv_after_21_days_organic,
              count(case when datediff(day,previa.created_at, global_orders.created_at) <= 21 then global_orders.gmv_usd end) as orders_after_21_days_organic,
              case when orders_after_21_days_organic = 0 then 1 else 0 end as churn_21_organic,
              coalesce(sum(case when datediff(day,previa.created_at, global_orders.created_at) <= 28 then global_orders.gmv_usd else 0 end),0) as gmv_after_28_days_organic,
              count(case when datediff(day,previa.created_at, global_orders.created_at) <= 28 then global_orders.gmv_usd end) as orders_after_28_days_organic,
              case when orders_after_28_days_organic = 0 then 1 else 0 end as churn_28_organic

      from previa
      left join global_finances.global_orders on  global_orders.country = '{{params.country}}'
                      and global_orders.application_user_id = previa.application_user_id
                      and global_orders.order_state not ilike '%cance%'
                      and global_orders.organic IN ('ORGANIC PURE','ORGANIC ALLY')
                      and global_orders.created_at > previa.created_at
                      and global_orders.created_at <= previa.created_at + interval '28 days'
      group by 1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31,32,33,34,35,36,37,38,39,40,41,42,43,44,45,46,47,48,49,50,51,52,53,54,55,56

)

select     '{{params.country}}'                                     as country,
           created_at,
           city_name,
           order_id,
           vertical_group,
           vertical_sub_group,
           brand_id,
           store_id,
           gmv,
           gmv_usd,
           application_user_id,
           kustomer_created_at,
           kustomer_conversation_id,
           level_1,
           level_2,
           exit_tipification,
           nss_x_ratings,
           no_of_ratings,
           is_automated,
           reopen,
           ticketAutomationExitRule,
           iteration, 
           ticket_type_nodes,
           new_key,
           automatic_solution_type,
           coalesce(ticket_type,'Sin Ticket')               as ticket_type,
           count_to_gmv,
           case
            when loyalty in ('DIAMANTE') then 'DIAMOND'
            when loyalty in ('OURO', 'ORO') then 'GOLD'
            when loyalty in ('PRATA', 'PLATA')  then 'SILVER'
            when loyalty in ('BRONZE', 'BRONCE')  then 'BRONZE'
                end                                         as loyalty,
           case 
            when rfm_segment in ('Diamond', 'Diamond RFM') then 'Diamond RFM'
            when rfm_segment in ('Undefined Segment RFM', 'Unidentified') then 'Undefined Segment RFM'
                 else rfm_segment end as rfm_segment,
           finance_user_type, 
           operation_type,

           case 
                when operation_type = 'Automation' and automatic_compensation_usd > 0 then 'DM Aprobó y compensó'
                when operation_type = 'Automation + Manual' and reopen > 0 and agent_compensation_usd > 0 and (automatic_compensation_usd = 0 or automatic_compensation_usd is null)  then 'DM Rechazó y compensó manual' 
                when operation_type = 'Automation + Manual' and reopen > 0 and automatic_compensation_usd > 0 and agent_compensation_usd > 0 then 'DM Aprobó compensó y agente compensó manual' 
                when operation_type = 'Automation + Manual' and automatic_compensation_usd > 0 and agent_compensation_usd > 0 then 'DM Aprobó compensó y agente compensó manual'
                when operation_type = 'Automation' and automatic_compensation_usd = 0 and agent_compensation_usd = 0 and compensation_value_usd = 0 then 'DM Aprobó y no compensó'
                when operation_type = 'Manual' and agent_compensation_usd > 0 then 'Manual Aprobó y compensó'
                when operation_type = 'Manual' and agent_compensation_usd = 0 then 'Manual no compensó'
                when kustomer_conversation_id is null then 'Sin Ticket'
                else 'Raro' end                             as tipologia,


           compensation_value_local,
           compensation_value_usd,
           agent_compensation_usd,
           agent_compensation_local,
           automatic_compensation_usd,
           automatic_compensation_local,

           Rappi_credits_debited,
           Ally_Compensations,
           Ally_Compensations_Adjustment,
           Order_modification_discounts,
           Rappi_Credits_not_Debited,
           max_transaction_created_at_refunds,
           Refunds,
           team,

           gmv_after_7_days,
           orders_after_7_days,
           churn_7,
           gmv_after_14_days,
           orders_after_14_days,
           churn_14,
           gmv_after_21_days,
           orders_after_21_days,
           churn_21,
           gmv_after_28_days,
           orders_after_28_days,
           churn_28,

           gmv_after_7_days_organic,
           orders_after_7_days_organic,
           churn_7_organic,
           gmv_after_14_days_organic,
           orders_after_14_days_organic,
           churn_14_organic,
           gmv_after_21_days_organic,
           orders_after_21_days_organic,
           churn_21_organic,
           gmv_after_28_days_organic,
           orders_after_28_days_organic,
           churn_28_organic,

           count(1)                                         orders,
           count(distinct application_user_id)              users,
           count(distinct kustomer_conversation_id)         tickets


       from previa_final
        group by 1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31,32,33,34,35,36,37,38,39,40,41,42,43,44,45,46,47,48,49,50,51,52,53,54,55,56,57,58,59,60,61,62,63,64,65,66,67,68,69,70
      qualify row_number() over (partition by order_id order by created_at) = 1
UNION ALL

select * from recovery

) as source

on target.country = source.country 
    and target.application_user_id = source.application_user_id 
    and target.order_id = source.order_id
when not matched then
  insert (
            country, 
            created_at,
            city_name,
            order_id,
            vertical_group,
            vertical_sub_group,
            brand_id,
            store_id,
            gmv,
            gmv_usd,
            application_user_id,
            kustomer_created_at,
            kustomer_conversation_id,
            level_1,
            level_2,
            exit_tipification,
            nss_x_ratings,
            no_of_ratings,
            is_automated,
            reopen,
            ticketAutomationExitRule,
            iteration, 
            ticket_type_nodes,
            key_,
            automatic_solution_type,
            ticket_type,
            count_to_gmv,
            loyalty,
            rfm_segment,
            finance_user_type, 
            operation_type,
            tipologia,
            compensation_value_local,
            compensation_value_usd,
            agent_compensation_usd,
            agent_compensation_local,
            automatic_compensation_usd,
            automatic_compensation_local,

            Rappi_credits_debited,
            Ally_Compensations,
            Ally_Compensations_Adjustment,
            Order_modification_discounts,
            Rappi_Credits_not_Debited,
            max_transaction_created_at_refunds,
            Refunds,
            team,

            gmv_after_7_days,
            orders_after_7_days,
            churn_7,
            gmv_after_14_days,
            orders_after_14_days,
            churn_14,
            gmv_after_21_days,
            orders_after_21_days,
            churn_21,
            gmv_after_28_days,
            orders_after_28_days,
            churn_28,

            gmv_after_7_days_organic,
            orders_after_7_days_organic,
            churn_7_organic,
            gmv_after_14_days_organic,
            orders_after_14_days_organic,
            churn_14_organic,
            gmv_after_21_days_organic,
            orders_after_21_days_organic,
            churn_21_organic,
            gmv_after_28_days_organic,
            orders_after_28_days_organic,
            churn_28_organic,

            orders,
            users,
            tickets

  )

values (
            source.country, 
            source.created_at,
            source.city_name,
            source.order_id,
            source.vertical_group,
            source.vertical_sub_group,
            source.brand_id,
            source.store_id,
            source.gmv,
            source.gmv_usd,
            source.application_user_id,
            source.kustomer_created_at,
            source.kustomer_conversation_id,
            source.level_1,
            source.level_2,
            source.exit_tipification,
            source.nss_x_ratings,
            source.no_of_ratings,
            source.is_automated,
            source.reopen,
            source.ticketAutomationExitRule,
            source.iteration, 
            source.ticket_type_nodes,
            source.key_,
            source.automatic_solution_type,
            source.ticket_type,
            source.count_to_gmv,
            source.loyalty,
            source.rfm_segment,
            source.finance_user_type, 
            source.operation_type,
            source.tipologia,
            source.compensation_value_local,
            source.compensation_value_usd,
            source.agent_compensation_usd,
            source.agent_compensation_local,
            source.automatic_compensation_usd,
            source.automatic_compensation_local,
            source.Rappi_credits_debited,
            source.Ally_Compensations,
            source.Ally_Compensations_Adjustment,
            source.Order_modification_discounts,
            source.Rappi_Credits_not_Debited,
            source.max_transaction_created_at_refunds,
            source.Refunds,
            source.team,
            source.gmv_after_7_days,
            source.orders_after_7_days,
            source.churn_7,
            source.gmv_after_14_days,
            source.orders_after_14_days,
            source.churn_14,
            source.gmv_after_21_days,
            source.orders_after_21_days,
            source.churn_21,
            source.gmv_after_28_days,
            source.orders_after_28_days,
            source.churn_28,
            source.gmv_after_7_days_organic,
            source.orders_after_7_days_organic,
            source.churn_7_organic,
            source.gmv_after_14_days_organic,
            source.orders_after_14_days_organic,
            source.churn_14_organic,
            source.gmv_after_21_days_organic,
            source.orders_after_21_days_organic,
            source.churn_21_organic,
            source.gmv_after_28_days_organic,
            source.orders_after_28_days_organic,
            source.churn_28_organic,
            source.orders,
            source.users,
            source.tickets
)   

when matched then update set

                        target.country = source.country, 
                        target.created_at = source.created_at,
                        target.city_name = source.city_name,
                        target.order_id = source.order_id,
                        target.vertical_group = source.vertical_group,
                        target.vertical_sub_group = source.vertical_sub_group,
                        target.brand_id = source.brand_id,
                        target.store_id = source.store_id,
                        target.gmv = source.gmv,
                        target.gmv_usd = source.gmv_usd,
                        target.application_user_id = source.application_user_id,
                        target.kustomer_created_at = source.kustomer_created_at,
                        target.kustomer_conversation_id = source.kustomer_conversation_id,
                        target.level_1 = source.level_1,
                        target.level_2 = source.level_2,
                        target.exit_tipification = source.exit_tipification,
                        target.nss_x_ratings = source.nss_x_ratings,
                        target.no_of_ratings = source.no_of_ratings,
                        target.is_automated = source.is_automated,
                        target.reopen = source.reopen,
                        target.ticketAutomationExitRule = source.ticketAutomationExitRule,
                        target.iteration, = source.iteration, 
                        target.ticket_type_nodes = source.ticket_type_nodes,
                        target.key_ = source.key_,
                        target.automatic_solution_type = source.automatic_solution_type,
                        target.ticket_type = source.ticket_type,
                        target.count_to_gmv = source.count_to_gmv,
                        target.loyalty = source.loyalty,
                        target.rfm_segment = source.rfm_segment,
                        target.finance_user_type, = source.finance_user_type, 
                        target.operation_type = source.operation_type,
                        target.tipologia = source.tipologia,
                        target.compensation_value_local = source.compensation_value_local,
                        target.compensation_value_usd = source.compensation_value_usd,
                        target.agent_compensation_usd = source.agent_compensation_usd,
                        target.agent_compensation_local = source.agent_compensation_local,
                        target.automatic_compensation_usd = source.automatic_compensation_usd,
                        target.automatic_compensation_local = source.automatic_compensation_local,
                        target.Rappi_credits_debited = source.Rappi_credits_debited,
                        target.Ally_Compensations = source.Ally_Compensations,
                        target.Ally_Compensations_Adjustment = source.Ally_Compensations_Adjustment,
                        target.Order_modification_discounts = source.Order_modification_discounts,
                        target.Rappi_Credits_not_Debited = source.Rappi_Credits_not_Debited,
                        target.max_transaction_created_at_refunds = source.max_transaction_created_at_refunds,
                        target.Refunds = source.Refunds,
                        target.team = source.team,
                        target.gmv_after_7_days = source.gmv_after_7_days,
                        target.orders_after_7_days = source.orders_after_7_days,
                        target.churn_7 = source.churn_7,
                        target.gmv_after_14_days = source.gmv_after_14_days,
                        target.orders_after_14_days = source.orders_after_14_days,
                        target.churn_14 = source.churn_14,
                        target.gmv_after_21_days = source.gmv_after_21_days,
                        target.orders_after_21_days = source.orders_after_21_days,
                        target.churn_21 = source.churn_21,
                        target.gmv_after_28_days = source.gmv_after_28_days,
                        target.orders_after_28_days = source.orders_after_28_days,
                        target.churn_28 = source.churn_28,
                        target.gmv_after_7_days_organic = source.gmv_after_7_days_organic,
                        target.orders_after_7_days_organic = source.orders_after_7_days_organic,
                        target.churn_7_organic = source.churn_7_organic,
                        target.gmv_after_14_days_organic = source.gmv_after_14_days_organic,
                        target.orders_after_14_days_organic = source.orders_after_14_days_organic,
                        target.churn_14_organic = source.churn_14_organic,
                        target.gmv_after_21_days_organic = source.gmv_after_21_days_organic,
                        target.orders_after_21_days_organic = source.orders_after_21_days_organic,
                        target.churn_21_organic = source.churn_21_organic,
                        target.gmv_after_28_days_organic = source.gmv_after_28_days_organic,
                        target.orders_after_28_days_organic = source.orders_after_28_days_organic,
                        target.churn_28_organic = source.churn_28_organic,
                        target.orders = source.orders,
                        target.users = source.users,
                        target.ticket = source.tickets
